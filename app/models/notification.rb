class Notification < ActiveRecord::Base
  belongs_to :terminal
  belongs_to :prefetchable

  before_save :set_collapse_key

  private

  def set_collapse_key
    self.collapse_key = Time.now.to_i.to_s + self.terminal.id.to_s
  end
end
