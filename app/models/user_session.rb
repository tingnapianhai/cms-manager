class UserSession < Authlogic::Session::Base
  def to_key
    new_record? ? nil : [self.send(self.class.primary_key) ]
  end

  def persisted?
    false
  end

  #oauth2_client_id      "256004954427982"
  #oauth2_client_secret  "f6b2908daf03a679e76fe6b460df6a85"
  #oauth2_site           "https://graph.facebook.com"
  #oauth2_scope          "offline_access,email,read_stream"
  #oauth2_cannot_find_record_message "We have no record of you in our database, please sign up first."

  #def self.oauth_consumer
  #  OAuth::Consumer.new("256004954427982", "f6b2908daf03a679e76fe6b460df6a85",
  #                      { :site => "http://facebook.com",
  #                        :authorize_url => "https://www.facebook.com/dialog/oauth"      
  #  })
  #end

end
