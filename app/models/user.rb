class User < ActiveRecord::Base
  acts_as_authentic
  has_many :terminals, :dependent => :destroy
  has_many :authentications, :dependent => :destroy
  has_many :videos, :dependent => :destroy
  has_many :subscriptions, :dependent => :destroy
  has_many :providers, :through => :subscriptions

  def graph
    provider = self.authentications.find_by_provider("facebook")
    @graph ||= provider ? Koala::Facebook::API.new(provider["credential"]) : nil
    return @graph
  end

  def facebook?
    return !self.authentications.find_by_provider("facebook").nil?
  end

end
