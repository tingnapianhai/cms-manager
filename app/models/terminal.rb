class Terminal < ActiveRecord::Base
  has_many :prefetchables, :dependent => :destroy
  belongs_to :user
  has_many :notifications, :dependent => :destroy
  has_many :interactions, :dependent => :destroy

  after_commit :setname, :setuid
  def Terminal.getquotaocc(user,terminal)
    if user.cycled.day > Date.today.day
      quota = Transfer.where(:prefetchable_id => Prefetchable.find(:all, :conditions => ["terminal_id=?",terminal.id])).where('connection_type !=?',99).where('start BETWEEN ? AND ?',Date.new(Date.today.year,(Date.today - 1.month).month,user.cycled.day),Date.new(Date.today.year,Date.today.month,user.cycled.day)).find(:all,:select => "bits").sum(&:bits)/1048576
      wifi = Transfer.where(:prefetchable_id => Prefetchable.find(:all, :conditions => ["terminal_id=?",terminal.id])).where('connection_type =?',99).where('start BETWEEN ? AND ?',Date.new(Date.today.year,(Date.today - 1.month).month,user.cycled.day),Date.new(Date.today.year,Date.today.month,user.cycled.day)).find(:all,:select => "bits").sum(&:bits)/1048576
    else
      quota = Transfer.where(:prefetchable_id => Prefetchable.find(:all, :conditions => ["terminal_id=?",terminal.id])).where('connection_type !=?',99).where('start BETWEEN ? AND ?',Date.new(Date.today.year,Date.today.month,user.cycled.day),Date.new(Date.today.year,(Date.today + 1.month).month,user.cycled.day)).find(:all,:select => "bits").sum(&:bits)/1048576
      wifi = Transfer.where(:prefetchable_id => Prefetchable.find(:all, :conditions => ["terminal_id=?",terminal.id])).where('connection_type =?',99).where('start BETWEEN ? AND ?',Date.new(Date.today.year,Date.today.month,user.cycled.day),Date.new(Date.today.year,(Date.today + 1.month).month,user.cycled.day)).find(:all,:select => "bits").sum(&:bits)/1048576
    end
    return quota,wifi
  end

  private
  
  def setname
    if self.name == "" || self.name.nil?
      self.name = "Terminal #{self.id}"
      self.save
    end
  end

  def setuid
    if self.uid == "" || self.uid.nil?
      self.uid = "#{self.id}"
      self.save
    end
  end

end
