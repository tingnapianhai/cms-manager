class ContentlistsController < ApplicationController
  before_filter :get_poster
  # GET /contentlists
  # GET /contentlists.xml
  def index
    @contentlists = @poster.contentlists

    respond_to do |format|
      format.html # index.html.erb
      #format.xml  { render :xml => @contentlists }
    end
  end

  # GET /contentlists/1
  # GET /contentlists/1.xml
  #def show
  #  @contentlist = @poster.contentlists.find(params[:id])

  #  respond_to do |format|
  #    format.html # show.html.erb
  #    format.xml  { render :xml => @contentlist }
  #  end
  #end

  # GET /contentlists/new
  # GET /contentlists/new.xml
  def new
    #@contentlist = Contentlist.new
    @contentlist = @poster.contentlists.new

    respond_to do |format|
      format.html # new.html.erb
      #format.xml  { render :xml => @contentlist }
    end
  end

  # GET /contentlists/1/edit
  def edit
    @contentlist = @poster.contentlists.find(params[:id])
  end

  # POST /contentlists
  # POST /contentlists.xml
  def create
    @contentlist = @poster.contentlists.new(params[:contentlist])

    respond_to do |format|
      if @contentlist.save
        format.html { redirect_to(poster_contentlists_path, :notice => 'Contentlist was successfully created.') }
        #format.xml  { render :xml => @contentlist, :status => :created, :location => @contentlist }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @contentlist.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /contentlists/1
  # PUT /contentlists/1.xml
  def update
    @contentlist = @poster.contentlists.find(params[:id])

    respond_to do |format|
      if @contentlist.update_attributes(params[:contentlist])
        format.html { redirect_to(poster_contentlists_path, :notice => 'Contentlist was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @contentlist.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /contentlists/1
  # DELETE /contentlists/1.xml
  def destroy
    @contentlist = @poster.contentlists.find(params[:id])
    @contentlist.destroy

    respond_to do |format|
      format.html { redirect_to(poster_contentlists_path) }
      format.xml  { head :ok }
    end
  end
  def get_poster
    @poster = Poster.find(params[:poster_id])
  end
end
