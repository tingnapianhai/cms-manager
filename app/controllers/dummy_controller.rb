class DummyController < ApplicationController
  def parse_youtube(url)
   regex = /^(?:https?:\/\/)?(?:www\.)?\w*\.\w*\/(?:watch\?v=)?((?:p\/)?[\w\-]+)/
   url.match(regex)[1]
  end
  
  def getcontent
    @direction = params[:direction]
    @beacon = params[:beacon]
    poster = Poster.find(:all , :conditions => {:direction => @direction})[0]
    #poster = Poster.find(:all , :conditions => {:direction => @direction, :beacon => @beacon})[0]
    content = poster.contentlists
    poster.phoneport.to_i
    poster.serverport.to_i
    #resp = {
    #   :serverip =>  poster.serverip,
    #   :serverport  poster.serverport.to_i,
    #   :phoneport  poster.phoneport.to_i,
    #   :contentlist  content
    #}
    #poster = Poster.find(:all, :conditions => ["direction=?",@direction])
    respond_to do |format|
      #format.json {render :json => poster.to_json  }
      format.json {render :json => {
          :poster => poster || {},
          :content => content
        }
      }
    end
    #head :ok
  end 

  def getfriend
    @id = params[:url]
    @user_id = params[:user_id]
    uri = Video.find(:all, :select => "url,friend_name", :conditions =>["user_id=?",@user_id])
    urls = uri.map{|x| parse_youtube x.url}
    #@terminal = "sdfasd"
    respond_to do |format|
      if urls.index(@id)
        format.json {render :json => uri[urls.index(@id)].friend_name}
      else
        format.json {render :json => "Predicted"}
      end
    end

  end
end
