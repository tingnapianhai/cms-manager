class UserSessionsController < ApplicationController
  # GET /user_session/new
  def new
    @user_session = UserSession.new
  end

  # POST /user_session
  def create
    @user_session = UserSession.new(params[:user_session]) 

    @user_session.save do |result|
      if result
        respond_to do |format|
          format.html {redirect_to(:users, :notice => 'Login successful!')}
          format.json {render :json => {:success => true, :id => current_user.id} }
        end
      else
        respond_to do |format|
          format.html {render :action => "new"}
          format.json {render :json => {:success => false, :message => "Something went wrong. It couldn't login"}}
        end
      end
    end

  end

  # DELETE /user_session/1
  def destroy
    @user_session = UserSession.find
    if @user_session
      @user_session.destroy
      respond_to do |format|
        format.html {redirect_to(:users, :notice => "Goodbye!")}
        format.json {render :json => {:success => true, :message => "User logged out"}}
      end
    else
      respond_to do |format|
        format.html {redirect_to(:users, :notice => "The user was not logged in!")}
        format.json {render :json => {:success => false, :message => "The user was not logged in"}}
      end
    end
    
  end

end
