class InteractionsController < ApplicationController
  before_filter :get_user, :get_terminal

  def index
    @interactions = @terminal.interactions.find(:all, :order => "created_at DESC")

    respond_to do |format|
      format.html # index.html.erb
      format.json {render :json => {:interactions => @interactions}}
    end
  end

  def show
    @interaction = @terminal.interactions.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  def new
    @interaction = @terminal.interactions.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  def edit
    @interaction = @terminal.interactions.find(params[:id])
  end

  def create
    @interaction = @terminal.interactions.new(params[:interaction])

    respond_to do |format|
      if @interaction.save
        format.html { redirect_to(user_terminal_interactions_path, :notice => 'Interaction was successfully created.') }
        format.json  { render :json => {:sucess => true, :interaction_id => @interaction.id}}
      else
        format.html { render :action => "new" }
        format.json  { render :json => {:success => false} }
      end
    end
  end

  def update
    @interaction = @terminal.interactions.find(params[:id])

    respond_to do |format|
      if @interaction.update_attributes(params[:interaction])
        format.html { redirect_to(user_terminal_interactions_path, :notice => 'Interaction was successfully updated.') }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @interaction = @terminal.interactions.find(params[:id])
    @interaction.destroy

    respond_to do |format|
      format.html { redirect_to(user_terminal_interactions_url) }
    end
  end

  private

  def get_user
    @user = User.find(params[:user_id])
  end

  def get_terminal
    @terminal = Terminal.find(params[:terminal_id])
  end

end
