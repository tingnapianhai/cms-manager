class TerminalsController < ApplicationController
  before_filter :get_user

  def index
    @terminals = @user.terminals.all

    respond_to do |format|
      format.html # index.html.erb
      format.json {render :json => {:terminals => @terminals}}
    end
  end

  def show
    @terminal = @user.terminals.find(params[:id])

    respond_to do |format|
      format.html
      format.json {render :json => @terminal}
    end
  end

  def new
    @terminal = @user.terminals.new

    respond_to do |format|
      format.html
    end
  end

  def edit
    @terminal = @user.terminals.find(params[:id])
  end

  def create
    #Check by UID if the terminal was already created
    @terminal = @user.terminals.find_by_uid(params[:terminal][:uid])
    if @terminal
      @terminal.update_attributes(params[:terminal])
    else
      @terminal = @user.terminals.build(params[:terminal])
    end

    respond_to do |format|
      if @terminal.save
        format.html { redirect_to([@user, @terminal], :notice => 'Terminal was successfully created.') }
        format.json  { render :json => {:success => true, :terminal_id => @terminal.id} }
      else
        puts @terminal
        format.html { render :action => "new" }
        format.json {render :json => {:success => false, :message => "Something went wrong. It couldn't create terminal"}}
      end
    end
  end

  def update
    @terminal = @user.terminals.find(params[:id])

    respond_to do |format|
      if @terminal.update_attributes(params[:terminal])
        format.html { redirect_to([@user, @terminal], :notice => 'Terminal was successfully updated.') }
        format.json  { render :json => {:success => true} }
      else
        format.html { render :action => "edit" }
        format.json {render :json => {:success => false, :message => "Something went wrong. It couldn't update terminal"}}
      end
    end
  end

  def destroy
    respond_to do |format|
      begin
        @terminal = @user.terminals.find(params[:id])
        @terminal.destroy
        format.html { redirect_to(user_terminals_path(@user)) }
        format.json { render :json => {:success => true} }
      rescue
        format.html { redirect_to(user_terminals_path(@user)) }
        format.json { render :json => {:success => false} }
      end
    end
  end

  private

  def get_user
    @user = User.find(params[:user_id])
  end
end
