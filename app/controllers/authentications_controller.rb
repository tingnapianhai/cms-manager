class AuthenticationsController < ApplicationController
  before_filter :get_user

  def index
    @authentications = @user.authentications
  end

  def show
    @authentication = Authentication.find(params[:id])
  end

  def edit
    @authentication = @user.authentications.find(params[:id])
  end

  def new
    @authentication = @user.authentications.new
  end
  
  def create
    auth = request.env["omniauth.auth"]
    @authentication = @user.authentications.find_or_create_by_provider_and_uid(auth['provider'], auth['uid'])
    if @authentication.credential != auth["credentials"]["token"]
      @authentication.credential = auth["credentials"]["token"]
      @authentication.save
    end
    flash[:notice] = "Authentication successful"
    redirect_to user_authentication_url(@user, @authentication)
  end

  def destroy
    @authentication = @user.authentications.find(params[:id])
    @authentication.destroy
    flash[:notice] = "Destroyed authentication"
    redirect_to(user_authentications_url(@user))
  end

  private

  def get_user
    if params[:user_id]
      @user = User.find(params[:user_id]) 
    else
      @user = current_user
    end
  end
end
