require 'test_helper'

class VideosControllerTest < ActionController::TestCase

  setup do
    activate_authlogic
    @user = users(:one)
    @video = videos(:one)
  end

  test "should get index" do
    get :index, :user_id => @user.to_param
    assert_response :success
    assert_not_nil assigns(:videos)
  end

  test "should get new" do
    get :new, :user_id => @user.to_param
    assert_response :success
  end

  test "should create video" do
    assert_difference('Video.count') do
      post(:create, {
        :user_id => @user.to_param,
        :video => @video.attributes 
      })
    end

    assert_redirected_to user_video_path(@user, assigns(:video))
  end

  test "should show video" do
    get :show, :user_id => @user.to_param, :id => @video.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :user_id => @user.to_param, :id => @video.to_param
    assert_response :success
  end

  test "should update video" do
    put :update, :user_id => @user.to_param, :id => @video.to_param, :video => @video.attributes
    assert_redirected_to user_video_path(@user, assigns(:video))
  end

  test "should destroy video" do
    assert_difference('Video.count', -1) do
      delete :destroy, :user_id => @user.to_param, :id => @video.to_param
    end

    assert_redirected_to user_videos_path(@user)
  end
end
