require 'test_helper'

class TerminalsControllerTest < ActionController::TestCase
  setup do
    @terminal = terminals(:one)
    @user = users(:one)
  end

  test "should get index" do
    get :index, :user_id => @user.to_param
    assert_response :success
    assert_not_nil assigns(:terminals)
  end

  test "should get new" do
    get :new, :user_id => @user.to_param
    assert_response :success
  end

  test "should create terminal" do
    assert_difference('Terminal.count') do
      data = @terminal.attributes
      data["uid"] = "different uid"
      post :create, :terminal => data, :user_id => @user.to_param
    end

    assert_redirected_to user_terminal_path(@user, assigns(:terminal))
  end

  test "should not create terminal" do
    #no difference since the terminal uid already exists
    assert_no_difference('Terminal.count') do
      post :create, :terminal => @terminal.attributes, :user_id => @user.to_param
    end

    assert_redirected_to user_terminal_path(@user, assigns(:terminal))
  end

  test "should show terminal" do
    get :show, :id => @terminal.to_param, :user_id => @user.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @terminal.to_param, :user_id => @user.to_param
    assert_response :success
  end

  test "should update terminal" do
    put :update, :id => @terminal.to_param, :user_id => @user.to_param , :terminal => @terminal.attributes
    assert_redirected_to user_terminal_path(@user, assigns(:terminal))
  end

  test "should destroy terminal" do
    assert_difference('Terminal.count', -1) do
      delete :destroy, :id => @terminal.to_param, :user_id => @user.to_param
    end

    assert_redirected_to user_terminals_path(@user)
  end
end
