require 'test_helper'

class PrefetchablesControllerTest < ActionController::TestCase
  setup do
    @prefetchable = prefetchables(:one)
    @terminal = terminals(:one)
    @user = users(:one)
    transfer_one = transfers(:one).attributes.clone
    transfer_one.delete("id")
    transfer_two = transfers(:two).attributes.clone
    transfer_two.delete("id")
    @state = {
      :transfers_attributes => {
        1234 => transfer_one,
        6789 => transfer_two
      },
      :energy_pc => 0.98,
      :energy_ma => -12456,
      :status => 0
    }
  end

  test "should get index" do
    get :index, :user_id => @user.to_param, :terminal_id => @terminal.to_param
    assert_response :success
    assert_not_nil assigns(:prefetchables)
  end

  test "should get new" do
    get :new, :user_id => @user.to_param, :terminal_id => @terminal.to_param
    assert_response :success
  end

  test "should create prefetchable" do
    assert_difference('Prefetchable.count') do
      post :create, :user_id => @user.to_param, :terminal_id => @terminal.to_param, :prefetchable => @prefetchable.attributes
    end

    assert_redirected_to user_terminal_prefetchable_path(@user, @terminal, assigns(:prefetchable))
  end

  test "should show prefetchable" do
    get :show, :id => @prefetchable.to_param, :user_id => @user.to_param, :terminal_id => @terminal.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @prefetchable.to_param, :user_id => @user.to_param, :terminal_id => @terminal.to_param
    assert_response :success
  end

  test "should update prefetchable" do
    put :update, :id => @prefetchable.to_param, :prefetchable => @prefetchable.attributes, :user_id => @user.to_param, :terminal_id => @terminal.to_param
    assert_redirected_to user_terminal_prefetchable_path(@user, @terminal, assigns(:prefetchable))
  end

  test "should destroy prefetchable" do
    assert_difference('Prefetchable.count', -1) do
      delete :destroy, :id => @prefetchable.to_param, :user_id => @user.to_param, :terminal_id => @terminal.to_param
    end

    assert_redirected_to user_terminal_prefetchables_path(@user, @terminal)
  end

  test "should update prefetchable state" do

    assert_difference('Transfer.count', 2) do
      post :state, :id => @prefetchable.to_param, :user_id => @user.to_param, :terminal_id => @terminal.to_param, :prefetchable => @state, :format => :json
    end

    assert_equal 0, assigns(:prefetchable).status

    assert_response :success
  end

end
