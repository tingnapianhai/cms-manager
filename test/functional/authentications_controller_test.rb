require 'test_helper'

class AuthenticationsControllerTest < ActionController::TestCase
  setup do
    activate_authlogic
    @authentication = authentications(:one)
    @user = users(:one)
  end

  test "should get index" do
    get :index, :user_id => @user.to_param
    assert_response :success
    assert_not_nil assigns(:authentications)
  end

  test "should get new" do
    get :new, :user_id => @user.to_param
    assert_response :success
  end

  test "should create authentication" do
    request.stubs(:env).returns("omniauth.auth" => {"uid" => "123", "provider" => "facebook", "credentials" => {"token" => "12345678"}})
    assert_difference('Authentication.count') do
      post :create, :authentication => @authentication.attributes, :user_id => @user.to_param
    end

    assert_redirected_to user_authentication_path(@user, assigns(:authentication))
  end

  test "should show authentication" do
    get :show, :id => @authentication.to_param, :user_id => @user.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @authentication.to_param, :user_id => @user.to_param
    assert_response :success
  end

  test "should destroy authentication" do
    assert_difference('Authentication.count', -1) do
      delete :destroy, :id => @authentication.to_param, :user_id => @user.to_param
    end

    assert_redirected_to user_authentications_path(@user)
  end
end
