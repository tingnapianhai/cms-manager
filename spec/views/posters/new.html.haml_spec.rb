require 'spec_helper'

describe "posters/new" do
  before(:each) do
    assign(:poster, stub_model(Poster,
      :location => "MyText",
      :direction => "MyText"
    ).as_new_record)
  end

  it "renders new poster form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => posters_path, :method => "post" do
      assert_select "textarea#poster_location", :name => "poster[location]"
      assert_select "textarea#poster_direction", :name => "poster[direction]"
    end
  end
end
