require 'spec_helper'

describe "posters/index" do
  before(:each) do
    assign(:posters, [
      stub_model(Poster,
        :location => "MyText",
        :direction => "MyText"
      ),
      stub_model(Poster,
        :location => "MyText",
        :direction => "MyText"
      )
    ])
  end

  it "renders a list of posters" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
