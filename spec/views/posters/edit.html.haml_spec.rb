require 'spec_helper'

describe "posters/edit" do
  before(:each) do
    @poster = assign(:poster, stub_model(Poster,
      :location => "MyText",
      :direction => "MyText"
    ))
  end

  it "renders the edit poster form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => posters_path(@poster), :method => "post" do
      assert_select "textarea#poster_location", :name => "poster[location]"
      assert_select "textarea#poster_direction", :name => "poster[direction]"
    end
  end
end
