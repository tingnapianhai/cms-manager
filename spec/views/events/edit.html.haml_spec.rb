require 'spec_helper'

describe "events/edit" do
  before(:each) do
    @event = assign(:event, stub_model(Event,
      :eventType => "MyString",
      :eventDuration => 1,
      :interaction => nil
    ))
  end

  it "renders the edit event form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => events_path(@event), :method => "post" do
      assert_select "input#event_eventType", :name => "event[eventType]"
      assert_select "input#event_eventDuration", :name => "event[eventDuration]"
      assert_select "input#event_interaction", :name => "event[interaction]"
    end
  end
end
