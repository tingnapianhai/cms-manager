require 'spec_helper'

describe "events/new" do
  before(:each) do
    assign(:event, stub_model(Event,
      :eventType => "MyString",
      :eventDuration => 1,
      :interaction => nil
    ).as_new_record)
  end

  it "renders new event form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => events_path, :method => "post" do
      assert_select "input#event_eventType", :name => "event[eventType]"
      assert_select "input#event_eventDuration", :name => "event[eventDuration]"
      assert_select "input#event_interaction", :name => "event[interaction]"
    end
  end
end
