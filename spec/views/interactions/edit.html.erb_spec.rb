require 'spec_helper'

describe "interactions/edit" do
  before(:each) do
    @interaction = assign(:interaction, stub_model(Interaction,
      :terminal_id => 1,
      :url => "MyString",
      :cached => false,
      :cell => 1,
      :provider => "MyString",
      :duration => 1
    ))
  end

  it "renders the edit interaction form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => interactions_path(@interaction), :method => "post" do
      assert_select "input#interaction_terminal_id", :name => "interaction[terminal_id]"
      assert_select "input#interaction_url", :name => "interaction[url]"
      assert_select "input#interaction_cached", :name => "interaction[cached]"
      assert_select "input#interaction_cell", :name => "interaction[cell]"
      assert_select "input#interaction_provider", :name => "interaction[provider]"
      assert_select "input#interaction_duration", :name => "interaction[duration]"
    end
  end
end
