require 'spec_helper'

describe "interactions/index" do
  before(:each) do
    assign(:interactions, [
      stub_model(Interaction,
        :terminal_id => 1,
        :url => "Url",
        :cached => false,
        :cell => 1,
        :provider => "Provider",
        :duration => 1
      ),
      stub_model(Interaction,
        :terminal_id => 1,
        :url => "Url",
        :cached => false,
        :cell => 1,
        :provider => "Provider",
        :duration => 1
      )
    ])
  end

  it "renders a list of interactions" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Url".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => false.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Provider".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
