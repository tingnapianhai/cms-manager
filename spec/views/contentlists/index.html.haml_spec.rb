require 'spec_helper'

describe "contentlists/index" do
  before(:each) do
    assign(:contentlists, [
      stub_model(Contentlist,
        :name => "Name",
        :interactiontype => "Interactiontype",
        :contentid => "Contentid",
        :poster => nil
      ),
      stub_model(Contentlist,
        :name => "Name",
        :interactiontype => "Interactiontype",
        :contentid => "Contentid",
        :poster => nil
      )
    ])
  end

  it "renders a list of contentlists" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Interactiontype".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Contentid".to_s, :count => 2
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
