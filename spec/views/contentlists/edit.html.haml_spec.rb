require 'spec_helper'

describe "contentlists/edit" do
  before(:each) do
    @contentlist = assign(:contentlist, stub_model(Contentlist,
      :name => "MyString",
      :interactiontype => "MyString",
      :contentid => "MyString",
      :poster => nil
    ))
  end

  it "renders the edit contentlist form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => contentlists_path(@contentlist), :method => "post" do
      assert_select "input#contentlist_name", :name => "contentlist[name]"
      assert_select "input#contentlist_interactiontype", :name => "contentlist[interactiontype]"
      assert_select "input#contentlist_contentid", :name => "contentlist[contentid]"
      assert_select "input#contentlist_poster", :name => "contentlist[poster]"
    end
  end
end
