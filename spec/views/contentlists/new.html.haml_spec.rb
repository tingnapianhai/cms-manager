require 'spec_helper'

describe "contentlists/new" do
  before(:each) do
    assign(:contentlist, stub_model(Contentlist,
      :name => "MyString",
      :interactiontype => "MyString",
      :contentid => "MyString",
      :poster => nil
    ).as_new_record)
  end

  it "renders new contentlist form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => contentlists_path, :method => "post" do
      assert_select "input#contentlist_name", :name => "contentlist[name]"
      assert_select "input#contentlist_interactiontype", :name => "contentlist[interactiontype]"
      assert_select "input#contentlist_contentid", :name => "contentlist[contentid]"
      assert_select "input#contentlist_poster", :name => "contentlist[poster]"
    end
  end
end
