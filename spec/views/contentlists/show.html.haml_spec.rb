require 'spec_helper'

describe "contentlists/show" do
  before(:each) do
    @contentlist = assign(:contentlist, stub_model(Contentlist,
      :name => "Name",
      :interactiontype => "Interactiontype",
      :contentid => "Contentid",
      :poster => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Interactiontype/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Contentid/)
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(//)
  end
end
