require 'spec_helper'

describe "contents/edit" do
  before(:each) do
    @content = assign(:content, stub_model(Content,
      :provider_id => 1,
      :url => "MyString",
      :title => "MyString",
      :image => "MyString",
      :category => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit content form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => contents_path(@content), :method => "post" do
      assert_select "input#content_provider_id", :name => "content[provider_id]"
      assert_select "input#content_url", :name => "content[url]"
      assert_select "input#content_title", :name => "content[title]"
      assert_select "input#content_image", :name => "content[image]"
      assert_select "input#content_category", :name => "content[category]"
      assert_select "textarea#content_description", :name => "content[description]"
    end
  end
end
