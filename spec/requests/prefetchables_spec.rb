require "spec_helper"
describe "API Prefetchables" do

  before :each do
    @u = User.create! :username => "amigo", :email => "amigo@mio.com", :password => "pass", :"password_confirmation" => "pass"
    @t = Terminal.create! :name => "nexus_one", :user_id => @u.id
    @p = Prefetchable.create! :url => "youtube.com", :terminal_id => @t.id
  end

  it "shows valid prefetchable page" do
  get user_terminal_prefetchable_path(@u, @t, @p)
  assert_response :success
  assert assigns(:prefetchable)
  assert_select "table tr", 1 #header table for transfers
  end

  it "returns valid prefetchable JSON" do
    get user_terminal_prefetchable_path(@u, @t, @p, :format => :json)
    response.body.should == @p.to_json(:include => :conditions)
  end

end
