require "spec_helper"

describe "API" do

  before :each do
    @u = User.create! :username => "amigo", :email => "amigo@mio.com", :password => "pass", :"password_confirmation" => "pass"
    @t = Terminal.create! :name => "nexus_one", :user_id => @u.id
    @p = Prefetchable.create! :url => "youtube.com/...", :terminal_id => @t.id
  end

  it "can route to a prefetchable in JSON" do
    {:get => "/users/#{@u.to_param}/terminals/#{@t.to_param}/prefetchables/#{@p.to_param}.json"}.should route_to({:controller => "prefetchables", :action => "show", :format => "json", :id => @p.to_param, :user_id => @u.to_param, :terminal_id => @t.to_param})
  end

end
