require "spec_helper"

describe ContentlistsController do
  describe "routing" do

    it "routes to #index" do
      get("/contentlists").should route_to("contentlists#index")
    end

    it "routes to #new" do
      get("/contentlists/new").should route_to("contentlists#new")
    end

    it "routes to #show" do
      get("/contentlists/1").should route_to("contentlists#show", :id => "1")
    end

    it "routes to #edit" do
      get("/contentlists/1/edit").should route_to("contentlists#edit", :id => "1")
    end

    it "routes to #create" do
      post("/contentlists").should route_to("contentlists#create")
    end

    it "routes to #update" do
      put("/contentlists/1").should route_to("contentlists#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/contentlists/1").should route_to("contentlists#destroy", :id => "1")
    end

  end
end
