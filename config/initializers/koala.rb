#The reason for the configuration of SSL certifications is here: http://stackoverflow.com/questions/3977303/omniauth-facebook-certificate-verify-failed
if Rails.env.production?
  Koala.http_service.http_options = {:ssl => {:ca_file => "/etc/pki/tls/certs/ca-bundle.crt"}}
else
  Koala.http_service.http_options = {:ssl => {:ca_path => "/etc/ssl/certs"}}
end

