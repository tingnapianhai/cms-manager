#The reason for the configuration of SSL certifications is here: http://stackoverflow.com/questions/3977303/omniauth-facebook-certificate-verify-failed
if Rails.env.production?
  puts "Initializing omniauth in production environment"
  Rails.application.config.middleware.use OmniAuth::Builder do
    provider :facebook, '188514751225873', '1879f92030b42e41d11ea1665479c645', {:client_options => {:ssl => {:ca_file => "/etc/pki/tls/certs/ca-bundle.crt"}}, :scope => "read_stream, offline_access"}
  end
else
  Rails.application.config.middleware.use OmniAuth::Builder do
    provider :facebook, '256004954427982', 'f6b2908daf03a679e76fe6b460df6a85', {:client_options => {:ssl => {:ca_path => "/etc/ssl/certs"}}, :scope => "read_stream, offline_access"} 
  end
end
