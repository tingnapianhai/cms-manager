set :output, "log/cron_log.log"

every 1.minutes do
  rake "providers:pull"
end

every 1.minute do
  rake "prefetchables:confirm_notifications"
  rake "prefetchables:check_quota"
end

#Example:
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end

# Learn more: http://github.com/javan/whenever
