# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131017073401) do

  create_table "authentications", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "credential"
  end

  create_table "conditions", :force => true do |t|
    t.integer  "prefetchable_id"
    t.string   "name"
    t.string   "value"
    t.string   "operator"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contentlists", :force => true do |t|
    t.string   "name"
    t.string   "interactiontype"
    t.string   "contentid"
    t.integer  "poster_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "contents", :force => true do |t|
    t.integer  "provider_id"
    t.string   "url"
    t.string   "title"
    t.string   "image"
    t.string   "category"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", :force => true do |t|
    t.datetime "eventTime"
    t.string   "eventType"
    t.integer  "eventDuration"
    t.integer  "interaction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interactions", :force => true do |t|
    t.integer  "terminal_id"
    t.text     "url",          :limit => 255
    t.datetime "time"
    t.boolean  "cached"
    t.integer  "cell"
    t.string   "provider"
    t.integer  "duration"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "interruption"
  end

  create_table "notifications", :force => true do |t|
    t.integer  "terminal_id"
    t.integer  "prefetchable_id"
    t.string   "collapse_key"
    t.string   "data"
    t.boolean  "delay_while_idle"
    t.datetime "sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "posters", :force => true do |t|
    t.text     "location"
    t.text     "direction"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "beacon"
    t.string   "serverip"
    t.integer  "serverport"
    t.integer  "phoneport"
    t.string   "postername"
    t.string   "postertype"
  end

  create_table "prefetchables", :force => true do |t|
    t.integer  "terminal_id"
    t.datetime "expiration"
    t.integer  "priority"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "category"
    t.integer  "status"
    t.integer  "energy_ma"
    t.float    "energy_pc"
    t.integer  "content_id"
    t.string   "policy"
  end

  create_table "providers", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subscriptions", :force => true do |t|
    t.integer  "user_id"
    t.integer  "provider_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "terminals", :force => true do |t|
    t.string   "name"
    t.string   "os"
    t.text     "description"
    t.string   "registration_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "uid"
  end

  create_table "transfers", :force => true do |t|
    t.integer  "prefetchable_id"
    t.integer  "connection_type", :limit => 255
    t.datetime "start"
    t.datetime "finish"
    t.integer  "bits"
    t.integer  "cell"
    t.integer  "mcc"
    t.integer  "mnc"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "bssid"
  end

  create_table "users", :force => true do |t|
    t.string   "username"
    t.string   "email"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "oauth2_token"
    t.integer  "quota"
    t.date     "cycled"
  end

  add_index "users", ["oauth2_token"], :name => "index_users_on_oauth2_token"

  create_table "videos", :force => true do |t|
    t.integer  "user_id"
    t.string   "url"
    t.integer  "comments"
    t.integer  "likes"
    t.string   "title"
    t.datetime "date"
    t.integer  "shares"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "object_id"
    t.string   "friend_id"
    t.string   "friend_name"
    t.integer  "provider_id"
  end

end
