class AddProvideridToPrefetchable < ActiveRecord::Migration
  def self.up
    add_column :prefetchables, :provider_id, :integer
  end

  def self.down
    remove_column :prefetchables, :provider_id
  end
end
