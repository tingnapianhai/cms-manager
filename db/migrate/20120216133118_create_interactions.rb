class CreateInteractions < ActiveRecord::Migration
  def self.up
    create_table :interactions do |t|
      t.integer :terminal_id
      t.string :url
      t.datetime :time
      t.boolean :cached
      t.integer :cell
      t.string :provider
      t.integer :duration

      t.timestamps
    end
  end

  def self.down
    drop_table :interactions
  end
end
