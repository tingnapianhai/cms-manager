class AddServerportToPoster < ActiveRecord::Migration
  def self.up
    add_column :posters, :serverport, :number
  end

  def self.down
    remove_column :posters, :serverport
  end
end
