class AddObjectidToVideo < ActiveRecord::Migration
  def self.up
    add_column :videos, :object_id, :integer
  end

  def self.down
    remove_column :videos, :object_id
  end
end
