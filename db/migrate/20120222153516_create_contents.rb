class CreateContents < ActiveRecord::Migration
  def self.up
    create_table :contents do |t|
      t.integer :provider_id
      t.string :url
      t.string :title
      t.string :image
      t.string :category
      t.text :description

      t.timestamps
    end

    add_column :prefetchables, :content_id, :integer
    remove_column :prefetchables, :video_id
  end

  def self.down
    drop_table :contents
    add_column :prefetchables, :video_id, :integer
    remove_column :prefetchables, :content_id
  end
end
