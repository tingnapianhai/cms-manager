class ChangeUrlFromInteraction < ActiveRecord::Migration
  def self.up
    change_column :interactions, :url, :text
  end

  def self.down
    change_column :interactions, :url, :string
  end
end
