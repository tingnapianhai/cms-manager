class AddCycledToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :cycled, :date
  end

  def self.down
    remove_column :users, :cycled
  end
end
