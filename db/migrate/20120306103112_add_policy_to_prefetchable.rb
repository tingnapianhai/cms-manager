class AddPolicyToPrefetchable < ActiveRecord::Migration
  def self.up
    add_column :prefetchables, :policy, :string
  end

  def self.down
    remove_column :prefetchables, :policy
  end
end
