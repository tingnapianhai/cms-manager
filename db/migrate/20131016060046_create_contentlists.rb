class CreateContentlists < ActiveRecord::Migration
  def self.up
    create_table :contentlists do |t|
      t.string :name
      t.string :interactiontype
      t.string :contentid
      t.references :poster

      t.timestamps
    end
  end

  def self.down
    drop_table :contentlists
  end
end
