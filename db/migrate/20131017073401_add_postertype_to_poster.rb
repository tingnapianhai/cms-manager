class AddPostertypeToPoster < ActiveRecord::Migration
  def self.up
    add_column :posters, :postertype, :string
  end

  def self.down
    remove_column :posters, :postertype
  end
end
