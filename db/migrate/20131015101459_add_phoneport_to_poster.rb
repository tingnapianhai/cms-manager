class AddPhoneportToPoster < ActiveRecord::Migration
  def self.up
    add_column :posters, :phoneport, :number
  end

  def self.down
    remove_column :posters, :phoneport
  end
end
