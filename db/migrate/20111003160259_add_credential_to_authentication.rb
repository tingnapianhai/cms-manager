class AddCredentialToAuthentication < ActiveRecord::Migration
  def self.up
    add_column :authentications, :credential, :string
  end

  def self.down
    remove_column :authentications, :credential
  end
end
