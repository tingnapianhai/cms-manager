class ChangeSsFromTransfers < ActiveRecord::Migration
  def self.up
    remove_column :transfers, :ssid
    add_column :transfers, :bssid, :string
    change_column :transfers, :connection_type, :integer
  end

  def self.down
    remove_column :transfers, :bssid
    add_column :transfers, :ssid, :string
    change_column :transfers, :connection_type, :string
  end
end
