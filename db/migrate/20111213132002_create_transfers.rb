class CreateTransfers < ActiveRecord::Migration
  def self.up
    create_table :transfers do |t|
      t.integer :prefetchable_id
      t.string :connection_type
      t.datetime :start
      t.datetime :finish
      t.integer :bits
      t.integer :cell
      t.integer :mcc
      t.integer :mnc
      t.string :ssid

      t.timestamps
    end
  end

  def self.down
    drop_table :transfers
  end
end
