class CreateNotifications < ActiveRecord::Migration
  def self.up
    create_table :notifications do |t|
      t.integer :terminal_id
      t.integer :prefetchable_id
      t.string :collapse_key
      t.string :data
      t.boolean :delay_while_idle
      t.datetime :sent_at

      t.timestamps
    end
  end

  def self.down
    drop_table :notifications
  end
end
