class CreateEvents < ActiveRecord::Migration
  def self.up
    create_table :events do |t|
      t.timestamp :eventTime
      t.string :eventType
      t.integer :eventDuration
      t.references :interaction

      t.timestamps
    end
  end

  def self.down
    drop_table :events
  end
end
