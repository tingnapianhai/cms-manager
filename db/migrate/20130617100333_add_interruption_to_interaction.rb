class AddInterruptionToInteraction < ActiveRecord::Migration
  def self.up
    add_column :interactions, :interruption, :integer
  end

  def self.down
    remove_column :interactions, :interruption
  end
end
