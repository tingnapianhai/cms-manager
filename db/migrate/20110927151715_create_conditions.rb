class CreateConditions < ActiveRecord::Migration
  def self.up
    create_table :conditions do |t|
      t.integer :prefetchable_id
      t.string :name
      t.string :value
      t.string :operator

      t.timestamps
    end
    remove_column :prefetchables, :conditions
  end

  def self.down
    drop_table :conditions
    add_column :prefetchables, :conditions, :string
  end
end
