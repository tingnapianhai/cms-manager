class AddStatusToPrefetchable < ActiveRecord::Migration
  def self.up
    add_column :prefetchables, :status, :integer
    add_column :prefetchables, :energy_ma, :integer
    add_column :prefetchables, :energy_pc, :float
  end

  def self.down
    remove_column :prefetchables, :energy_pc
    remove_column :prefetchables, :energy_ma
    remove_column :prefetchables, :status
  end
end
