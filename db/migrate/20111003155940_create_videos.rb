class CreateVideos < ActiveRecord::Migration
  def self.up
    create_table :videos do |t|
      t.integer :user_id
      t.string :source
      t.string :url
      t.integer :comments
      t.integer :likes
      t.string :title
      t.datetime :date
      t.integer :shares

      t.timestamps
    end
  end

  def self.down
    drop_table :videos
  end
end
