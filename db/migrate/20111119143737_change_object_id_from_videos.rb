class ChangeObjectIdFromVideos < ActiveRecord::Migration
  def self.up
    change_column :videos, :object_id, :string
  end

  def self.down
    change_column :videos, :object_id, :integer
  end
end
