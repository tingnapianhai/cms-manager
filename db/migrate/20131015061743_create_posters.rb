class CreatePosters < ActiveRecord::Migration
  def self.up
    create_table :posters do |t|
      t.text :location
      t.text :direction

      t.timestamps
    end
  end

  def self.down
    drop_table :posters
  end
end
