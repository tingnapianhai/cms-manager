class AddCategoryToPrefetchable < ActiveRecord::Migration
  def self.up
    add_column :prefetchables, :category, :string
    remove_column :prefetchables, :type
  end

  def self.down
    remove_column :prefetchables, :category
    add_column :prefetchables, :type, :string
  end
end
