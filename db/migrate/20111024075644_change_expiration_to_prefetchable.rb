class ChangeExpirationToPrefetchable < ActiveRecord::Migration
  def self.up
    change_column :prefetchables, :expiration, :datetime
  end

  def self.down
    change_column :prefetchables, :expiration, :date
  end
end
