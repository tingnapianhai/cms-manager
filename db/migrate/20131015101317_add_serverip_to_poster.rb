class AddServeripToPoster < ActiveRecord::Migration
  def self.up
    add_column :posters, :serverip, :string
  end

  def self.down
    remove_column :posters, :serverip
  end
end
