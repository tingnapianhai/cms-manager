class CreatePrefetchables < ActiveRecord::Migration
  def self.up
    create_table :prefetchables do |t|
      t.string :url
      t.integer :terminal_id
      t.date :expiration
      t.string :conditions
      t.integer :type
      t.integer :priority

      t.timestamps
    end
  end

  def self.down
    drop_table :prefetchables
  end
end
