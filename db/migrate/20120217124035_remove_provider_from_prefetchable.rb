class RemoveProviderFromPrefetchable < ActiveRecord::Migration
  def self.up
    remove_column :prefetchables, :provider_id
  end

  def self.down
    add_column :prefetchables, :provider_id, :integer
  end
end
