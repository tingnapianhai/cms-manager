class AddFriendidToVideo < ActiveRecord::Migration
  def self.up
    add_column :videos, :friend_id, :string
    add_column :videos, :friend_name, :string
  end

  def self.down
    remove_column :videos, :friend_id
    remove_column :videos, :friend_name
  end
end
