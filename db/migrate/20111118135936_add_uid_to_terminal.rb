class AddUidToTerminal < ActiveRecord::Migration
  def self.up
    add_column :terminals, :uid, :string
  end

  def self.down
    remove_column :terminals, :uid
  end
end
