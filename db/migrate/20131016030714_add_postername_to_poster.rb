class AddPosternameToPoster < ActiveRecord::Migration
  def self.up
    add_column :posters, :postername, :string
  end

  def self.down
    remove_column :posters, :postername
  end
end
