class AddBeaconToPoster < ActiveRecord::Migration
  def self.up
    add_column :posters, :beacon, :string
  end

  def self.down
    remove_column :posters, :beacon
  end
end
