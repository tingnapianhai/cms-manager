class AddProvideridToVideo < ActiveRecord::Migration
  def self.up
    add_column :videos, :provider_id, :integer
    remove_column :videos, :source
  end

  def self.down
    remove_column :videos, :provider_id
    add_column :videos, :source, :string
  end
end
