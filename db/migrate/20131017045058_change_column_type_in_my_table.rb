class ChangeColumnTypeInMyTable < ActiveRecord::Migration
  def self.up
    change_column :posters, :phoneport, :integer
    change_column :posters, :serverport, :interger
  end

  def self.down
    change_column :posters, :phoneport, :number
    change_column :posters, :serverport, :number
  end
end
