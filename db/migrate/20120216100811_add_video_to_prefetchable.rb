class AddVideoToPrefetchable < ActiveRecord::Migration
  def self.up
    add_column :prefetchables, :video_id, :integer
    remove_column :prefetchables, :url
  end

  def self.down
    remove_column :prefetchables, :video_id
    add_column :prefetchables, :url, :string
  end
end
