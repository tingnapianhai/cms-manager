class AddUserToTerminal < ActiveRecord::Migration
  def self.up
    add_column :terminals, :user_id, :integer
  end

  def self.down
    remove_column :terminals, :user_id
  end
end
