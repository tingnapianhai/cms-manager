namespace :prefetchables do
  task :confirm_notifications => :environment do
    prefetchables = Prefetchable.find(:all, :conditions => ["status = ?", Prefetchable::NOTIFIED])
    prefetchables.each do |p|
      if p.expiration < Time.now
        p.status = Prefetchable::EXPIRED
        p.save
      else
        times_notified = p.notifications.count
        time_to_notify = 4 ** times_notified
        if ((Time.now - p.created_at)/60) > time_to_notify
          p.send_notification
          puts "#{Time.now.to_s}: Notification #{times_notified+1} sent to terminal #{p.terminal_id}"
        end
      end
    end
  end
  task :check_quota => :environment do 
    compulsory_pref = Prefetchable.find(:all, :conditions => ["category = ? AND status= ?", "compulsory",Prefetchable::WAITING])
    compulsory_pref.each do |p|
      if Terminal.getquotaocc(p.terminal.user,p.terminal)[0] >= p.first.terminal.user.quota 
        p.category = "normal"
        p.save
        p.send_notification
      end
    end
  end
end
