namespace :providers do
  
  task :pull => :environment do
    User.all.each do |user|
      p Time.now.to_s
      if user.providers.include? Provider.find_by_name("facebook")
        p "Loading all facebook videos for user '#{user.username}'"
        status, count, videos = Video.pull_facebook(user)
        if status == "OK"
          p "#{count} new Facebook videos found"
          videos.each do |video|
            c = video.to_content
            c.save
            user.terminals.each do |terminal|
              #p = c.to_prefetchable(video.priority?)
              p = c.to_prefetchable_basedby_accessratio(terminal,video)
              p.terminal = terminal
              if p.save
                p "Created and notified #{p.category} prefetchable: (id, user_id, terminal_id, content_id): (#{p.id}, #{user.id}, #{terminal.id}, #{c.id})"
              else
                p "Error creating prefetchable (user, terminal, content): (#{user.id}, #{terminal.id}, #{content.id})"
              end
            end
          end
        else #status != OK
          p "There was some error, couldn't load the videos"
        end
      else
        p "No content providers for user '#{user.username}'"
      end
    end
  end

end
