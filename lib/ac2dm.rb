require 'gdata'
require 'net/https'
require 'uri'

module Ac2dm
  def Ac2dm.send_notification(noty)
#   token = self.open
    headers = { "Content-Type" => "application/x-www-form-urlencoded;charset=UTF-8", 
                "Authorization" => "key=AIzaSyBbesEoQAjY5x9sUcpzmE7WlTpfFADDLgk" }

    message = "registration_id=#{noty.terminal.registration_id}&collapse_key=#{noty.collapse_key}"
    data = noty.data.split("&").map{|d| d.split("=")}
    data.each{|k,v| message << "&data.#{k}=#{URI.escape(v)}"}

    message += "&delay_while_idle" if noty.delay_while_idle

#   url = URI.parse('https://android.apis.google.com/c2dm/send')
    url = URI.parse('https://android.googleapis.com/gcm/send')
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true

    resp, dat = http.post(url.path, message, headers)

    Rails.logger.info "Notification sent to terminal #{noty.terminal.id}: #{message}"
    return {:code => resp.code.to_i, :message => dat} 
  end

  def Ac2dm.open
    client_login_handler = GData::Auth::ClientLogin.new('ac2dm', :account_type => 'HOSTED_OR_GOOGLE')
    token = client_login_handler.get_token('mslkth@gmail.com',
                                           'msl!@KTH',
                                           'melange')

    return token
  end
end # C2dm
